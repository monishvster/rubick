
# Rubick
-------------

1. [Introduction](#introduction)
2. [Technologies](#technologies)
3. [Mockups](#mockups)

## Introduction

This is a website that enables teams to work collaboratively and efficiently. It consists of 4 different sections: Task cards, Repo updates, Chat and Leaderboard. Leaderboard is the distinguishing feature of this app as it lets users to know there standing among their teammates and make them competitive.
Its work in progress at the moment.

## Technologies
- nodeJS
- Express
- DynamoDB
- AWS

## Mockups
- You can view mockups [here][1]

[1]: https://app.moqups.com/harshasaim09@gmail.com/pC4C81aepx/view
